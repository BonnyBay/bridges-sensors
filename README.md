# DATA ANALYTICS: Bridges sensors project

Predictive analysis using dataset of natural frequencies of bridges structures.
You can apply binary or multiclass analysis.
Here below, the example for some binary classification.

## Getting started

Here's a list of commands that you'll have to do to create datasets and run some data analytics algorithms.

First: remove all .csv files from <b>DICAM_TOWER_support_freq_transpose</b> directory.
Then, paste inside directory <b>DICAM_TOWER_support_freq_transpose</b> your dataset (not transposed) with natural frequencies. WARNING: your rows has to be features and your column has to be the samples (normaly it is the viceversa, so rows for samples and columns for features/attributes).

Execute this command to transpose, remove data errors and add labels:
```
cd bridges-sensors
virtualenv venv
source venv/bin/activate
pip -r install requirements.txt
cd bridges-sensors/initial_script/binary
bash createFinalCsv.sh

```
Move the new created file <b>FinalVersion_DICAM_TOWER_CONCAT</b> inside the <b>scripts_python</b> directory and set the name of this dataset inside your file that contain the algoritmh such as <b>classic_technique.py</b> or <b>neural_network_ff.py</b>.

Then run:

```
python classic_technique.py
```

or:

```
python neural_network_ff.py
```

## Notes
The accuracy results of the neural network for the binary classification, change in a range 1 to 4 when it is runned on the laboratory's gpu ad when it is runned on our cpu. This is probably due to the deterministic algorithms setted to false on the laboratory's machines, because this setting can be active with cuda.


## Authors and acknowledgment
Emanuele Fazzini & Michele Bonini

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program.  If not, see https://www.gnu.org/licenses/.



