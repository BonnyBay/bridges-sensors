from random import shuffle
from webbrowser import get
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split, KFold, StratifiedKFold
from sklearn.naive_bayes import GaussianNB
import numpy as np
from sklearn import decomposition
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import os
from sklearn.ensemble import RandomForestClassifier
import seaborn as sn
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import LabelEncoder
from sklearn import linear_model
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler, SMOTE
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn import preprocessing


# confusion matrix for binary classification and multi classification
def make_confusion_matrix(Y_test, y_predicted, title):
    # ------------------------ use the line below for binary classification ------------------------
    #label_categories = ['no', 'si'] #no or yes

    # ------------------------ use the line below for multiclass classification  ------------------------
    #label_categories = ['D_10', 'D_20', 'D_30', 'D_40', 'D_50', 'D_60', 'D_70', 'D_80', 'D_90', 'D_99']
    label_categories = ['D_3040', 'D_5060', 'D_7080', 'D_9099']
    #label_categories = ['D_30','D_40', 'D_50','D_60', 'D_70','D_80', 'D_90','D_99']

    conf_matrix = confusion_matrix(Y_test, y_predicted)
    cmd_obj = ConfusionMatrixDisplay(conf_matrix, display_labels=label_categories)
    cmd_obj.plot()
    cmd_obj.ax_.set(
                title=title, 
                xlabel='Predicted', 
                ylabel='True')

    plt.show()

# function that create and plot the scatter plot
def scatter_frequency(freq_choose_X, freq_choose_Y):
    df_no = dataset_label_encoded[dataset_label_encoded.y_encoded==0] #create a new dataset with only "no"
    df_yes = dataset_label_encoded[dataset_label_encoded.y_encoded==1 ]#create a new dataset with only "si"
    plt.scatter(df_no.iloc[:,freq_choose_X], df_no.iloc[:,freq_choose_Y], color='green', marker='+')
    plt.scatter(df_yes.iloc[:,freq_choose_X], df_yes.iloc[:,freq_choose_Y], color='blue', marker='.')
    plt.xlabel('frequency '+str(freq_choose_X))
    plt.ylabel('frequency '+str(freq_choose_Y))
    plt.show()


# Encode the class label using LabelEncoder()
def encode_label(df):
    y_predict = LabelEncoder()
    df['y_encoded'] = y_predict.fit_transform(df.iloc[:,len(df.columns)-1])
    return df

# Pre-Processing function: PCA (Principal Components Analysis)
def pca_(X_train, X_test, components):
    pca = decomposition.PCA(n_components=components)
    pca.fit(X_train)
    #round the explained variance
    u = [round(i*100,2) for i in pca.explained_variance_ratio_]
    print("pca.explained_variance_ratio: ",u)
    print("sum pca explained_variance_ratio ",np.sum(u))
    X_train_t = pca.transform(X_train)
    X_test_t = pca.transform(X_test)
    print(pd.DataFrame(pca.components_,columns=X_train.columns))
    plt.plot(pd.DataFrame(pca.components_,columns=X_train.columns))
    #plt.plot(pca.components_)
    return X_train_t, X_test_t

# Pre-Processing function: LDA (Linear Discriminant Analysis)
def lda_(X_train, X_test, y_train, components):
    lda = LinearDiscriminantAnalysis(n_components=components)
    lda.fit(X_train, y_train)
    X_train_t = lda.transform(X_train)
    X_test_t = lda.transform(X_test)

    return X_train_t, X_test_t


# Pre-Processing: L2 norm (APPLIED ON THE WHOLE DATASET)
def L2_norm(X):
    x_norm1 = np.linalg.norm(X, ord=2)
    x_normalized = X / x_norm1
    return x_normalized

# Pre-Processing: standardization with Z-score technique (ONLY FOR TRAINING TEST)
def standardization(X_train, X_val):
    X_train_mean = np.mean(X_train, axis=0) 
    X_train_std = np.std(X_train, axis=0)
    
    # Z-score algorithm
    X_train_s = (X_train - X_train_mean) / X_train_std
    X_val_s = (X_val - X_train_mean) / X_train_std

    return X_train_s, X_val_s

# Pre-Processing: Random Over Sample
def balancingUnder(X_train, Y_train):
    print("BEFORE BALANCING ")
    print(np.unique(Y_train, return_counts=True))
    undersample = RandomUnderSampler(sampling_strategy='majority')
    X_train, Y_train = undersample.fit_resample(X_train, Y_train)
    print("AFTER BALANCING UNDERSAMPLE")
    print(np.unique(Y_train, return_counts=True))
    return [X_train, Y_train]

# Pre-Processing: Random Under Sample
def balancingOver(X_train, Y_train):
    print("BEFORE BALANCING ")
    print(np.unique(Y_train, return_counts=True))
    oversample = RandomOverSampler(sampling_strategy='minority')
    X_train, Y_train = oversample.fit_resample(X_train, Y_train)
    print("AFTER BALANCING OVERSAMPLE")
    print(np.unique(Y_train, return_counts=True))

    return [X_train, Y_train]


# Pre-Processing: SMOTE balancing (over sample technique)
def smote_balancing(X, y):
        print(np.unique(y, return_counts=True))
        smote = SMOTE(random_state = 42)
        X, y = smote.fit_resample(X, y)
        print(np.unique(y, return_counts=True))
        return X, y

# GaussianNB() algorithms
def bayes_classifier(X_train, X_test, Y_train, Y_test):
    gnb = GaussianNB()
    gnb.fit(X_train, Y_train)
    y_predict = gnb.predict(X_test)
    print("Avg accuracy TEST: ", gnb.score(X_test, Y_test))
    # confusion matrix
    make_confusion_matrix(Y_test, y_predict, "Gaussian Naive Bayes")

# Random Forest Algorithms
def random_forest(X_train, X_test, Y_train, Y_test):

    # ***************************************************************** #
    # *************** GRID SEARCH used in training phase ************** #    
    # ***************************************************************** #
    # estimators = range(100,1500,100)
    # depths = range(4,20,4)
    # crit = ["gini", "entropy"]
    # parameters = []
    # for i in estimators:
    #     for j in depths:
    #         for h in crit:
    #             parameters.append([i,j,h])
    # param_accuracy = []
    # max_acc = 0
    # for param in parameters:
    # ***************************************************************** #
    # ************************ END GRID SEARCH ************************ #    
    # ***************************************************************** #

    #BEST PARAM ACCURACY [N_EST=1200, MAX_DEPTH=16, CRIT=GINI, ACC=0.9416666666666667]

    n_est = 1200
    depth = 16 
    crite = "gini"
    rf = RandomForestClassifier(n_estimators=n_est, max_depth=depth, criterion=crite)
    rf.fit(X_train,Y_train) 
    y_predicted = rf.predict(X_test)

    accTrain = rf.score(X_train,Y_train)
    accTest = rf.score(X_test,Y_test)
    #max_acc = acc if acc > max_acc else max_acc
    #param_accuracy.append([n_est,depth,crit,acc])
    print("est: ",n_est," depth: ",depth, "accuracyTrain: ",accTrain," accuracyTest: ", accTest)
    importances = rf.feature_importances_
    std = np.std([tree.feature_importances_ for tree in rf.estimators_], axis=0)
    forest_importances = pd.Series(importances)

    fig, ax = plt.subplots()
    forest_importances.plot.bar(ax=ax)
    ax.set_title("Feature importances random forest")
    ax.set_ylabel("Mean decrease in impurity")
    fig.tight_layout()
    # for el in param_accuracy:
    #     if max_acc in el:
    #         print(el)

    #make_confusion_matrix(Y_test, y_predicted, "Random Forest")


# SVM classifier (Support Vector Machine)
def svm_classifier(X_train, X_test, Y_train, Y_test):
    #kernels = ['linear', 'poly', 'rbf', 'sigmoid'] # used to find the best kernel
    kernels = ['linear','rbf']

    for k in kernels:
        model =SVC(kernel=k) # the default is RBF kernel
        model.fit(X_train, Y_train)
        y_pred = model.predict(X_test)
        print(k, " SVM accuracy: ", model.score(X_test, Y_test))
        
        
        
        #make_confusion_matrix(Y_test,y_pred,"SVM")


# KNN classifier (K-Nearest Neighbor)
def knn_classifier(X_train,X_test,Y_train,Y_test):
    #for n in range(1,16): # used for find the best 'n', the number of Nearest Neighbor
    n = 9
    neigh = KNeighborsClassifier(n_neighbors=n)
    neigh.fit(X_train,Y_train)
    res = neigh.predict(X_test)
    print(n, " Test set accuracy: {}".format(neigh.score(X_test,Y_test)))

    #make_confusion_matrix(Y_test, res, "KNN")


# Cross Validation using K-Fold technique
def kfold_validation(df_X, df_Y):
    kf = KFold(n_splits=8, random_state=42, shuffle=True) #we choose 8 folds
    scores_svm = []
    scores_rf = []
    scores_svm_nonLinear = []
    scores_bayes = []
    scores_knn = []
    
    # we loop on each fold returned from kf.split(df_X) getting their accuracy
    for train_index, test_index in kf.split(df_X): 
        #getting the our test/training set using the index that come from kf.split(df_X)
        X_train, X_test, Y_train, Y_test = df_X.iloc[train_index], df_X.iloc[test_index], df_Y.iloc[train_index], df_Y.iloc[test_index]
        
        scores_svm.append(round(get_accuracy(SVC(kernel='linear'), X_train, X_test, Y_train, Y_test),2))
        scores_svm_nonLinear.append(round(get_accuracy(SVC(), X_train, X_test, Y_train, Y_test),2))
        scores_rf.append(round(get_accuracy(RandomForestClassifier(n_estimators=30, random_state=242), X_train, X_test, Y_train, Y_test),2))
        scores_bayes.append(round(get_accuracy(GaussianNB(),X_train, X_test, Y_train, Y_test),2))
        scores_knn.append(round(get_accuracy(KNeighborsClassifier(n_neighbors=4), X_train, X_test, Y_train, Y_test),2))

    print("svm linear ", scores_svm)
    print("\nsvm non linear ",scores_svm_nonLinear)
    print("\nrandom forest ", scores_rf)
    print("\nnaive bayes ", scores_bayes)
    print("\nknn ", scores_knn)


# Function that return the accuracy of the model on the test set
def get_accuracy(model, X_train, X_test, Y_train, Y_test):
    Y_train = Y_train.values.ravel()
    model.fit(X_train, Y_train)
    return model.score(X_test, Y_test)


if __name__ == '__main__':

    ######################### SETTINGS THE DATASET TO USE AND THE TRAIN_TEST_SPLIT #########################
    #dataset = pd.read_csv("../binary_class_analysis/binary_class_no1020.csv", index_col=False,header=None)
    dataset = pd.read_csv("../binary_class_analysis/binaryclass_damage_dataset.csv", index_col=False,header=None)
    #dataset = pd.read_csv("../multiclass_analysis/multiclass_all_damage_grouped.csv", index_col=False,header=None)
    #dataset = pd.read_csv("../multiclass_analysis/multiclass_all_damage.csv", index_col=False,header=None)
    #dataset = pd.read_csv("../multiclass_analysis/multiclass_no1020.csv", index_col=False,header=None)
    
    dataset = pd.DataFrame(dataset) 

    #remove the first column
    dataset = dataset.iloc[:,1:]

    # encode the classification column, si = 1 & no = 0
    dataset_label_encoded = encode_label(dataset)
    # train test, -2 because the second-last column are string and last column are classes encoded
    df_X = pd.DataFrame(dataset_label_encoded.iloc[:,:-2])    
    df_Y = pd.DataFrame(dataset_label_encoded.iloc[:,-1])


    # --------------- DATA VISUALIZATION ---------------
    # fig, axs = plt.subplots(4,3)
    # fig.tight_layout()
    # sn.kdeplot(df_X[0], ax=axs[0][0], color='g', bw_adjust=2,label='Frequency 0')
    # sn.kdeplot(df_X[1], ax=axs[0][1], color='r',label='Frequency 1') # cumulative=True
    # sn.scatterplot(df_X[1],df_X[2],ax=axs[0][2])
    # sn.boxplot(df_X[0], whis=1.5, ax=axs[1][0])
    # sn.boxplot(df_X[1], whis=1.5, ax=axs[1][1])
    # sn.scatterplot(df_X[0],df_X[2],ax=axs[1][2])

    # sn.kdeplot(df_X[62], ax=axs[2][0], color='g', bw_adjust=2,label='Frequency 0')
    # sn.kdeplot(df_X[63], ax=axs[2][1], color='r',label='Frequency 1') # cumulative=True
    # sn.scatterplot(df_X[63],df_X[64],ax=axs[2][2])
    # sn.boxplot(df_X[62], whis=1.5, ax=axs[3][0])
    # sn.boxplot(df_X[63], whis=1.5, ax=axs[3][1])
    # sn.scatterplot(df_X[62],df_X[64],ax=axs[3][2])
    # plt.show()
    # --------------- END DATA VISUALIZATION ---------------

    # --------------- START PRE-PROCESSING ---------------
    # apply L2 normalization
    df_X = pd.DataFrame(L2_norm(np.array(df_X)))

    # Split dataset in train and test 
    X_train, X_test, Y_train, Y_test = train_test_split(df_X, df_Y, test_size=0.33, random_state=42)
    Y_train = Y_train.values.ravel()

    # Call the standardization function
    #X_train, X_test = standardization(X_train,X_test)

    # BALANCING the dataset 
    #X_train, Y_train = balancingOver(X_train,Y_train)
    #X_train, Y_train = balancingUnder(X_train,Y_train)
    #X_train, Y_train = smote_balancing(X_train,Y_train)
    

    # PCA explained variance has to be>= 95
    components = 10 # 20 because adding new components not worth for accuracy
    X_train, X_test = pca_(X_train, X_test, components)  

    #LDA
    components_lda = 1 # num classes - 1
    #X_train, X_test = lda_(X_train, X_test, Y_train, components_lda)
    
    # --------------- END PRE-PROCESSING ---------------


    ###########################################################
    #                      ALGORITHMS                         #
    ###########################################################    
    
    # Bayes Classifier
    bayes_classifier(X_train, X_test, Y_train, Y_test)

    # Random Forest Classifier
    #random_forest(X_train, X_test, Y_train, Y_test)

    # SVM classifier
    #svm_classifier(X_train, X_test, Y_train, Y_test)

    # KNN Clissifier
    #knn_classifier(X_train,X_test,Y_train,Y_test)

    # cross-validazion KFolds
    #kfold_validation(df_X,df_Y)

    # scatter plot to see the distribution between two frequencies
    #freq_choose_X = 10 #frequency in the scatterplot on X axis
    #freq_choose_Y = 55 #frequency in the scatterplot on Y axis
    #scatter_frequency(freq_choose_X,freq_choose_Y)

