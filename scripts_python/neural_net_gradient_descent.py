import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn import decomposition
import torch

#########################################################################
#           NEURAL NETWORK FEED FORWARD WITH GRADIENT DESCENT           #
#########################################################################


#encoding the classification label
def encode_label(df):
    y_predict = LabelEncoder()
    df['y_encoded'] = y_predict.fit_transform(df.iloc[:,len(df.columns)-1])
    return df


# define the model class feed forward
class Feedforward(torch.nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(Feedforward, self).__init__()
        self.input_size = input_size #input dimension
        self.hidden_size = hidden_size #layer dimension
        self.num_classes = num_classes
        #define the layers ---> use the 3 lines below if you don't want to use the sequential mode
        # self.fc1 = torch.nn.Linear(self.input_size, self.hidden_size) #layer lineare fully connected. A sto layer serve il numero di neuroni in input e in output
        # self.relu = torch.nn.ReLU() #ReLU activation function
        # self.fc2 = torch.nn.Linear(self.hidden_size, self.num_classes)
        modules_list = []
        modules_list.append(torch.nn.Linear(input_size, hidden_size))

        # ******** start sequential mode, add a layer using the layer_number variable ******** #
        layer_number = 5
        for i in range(0,layer_number): #layer_number min 1
            modules_list.append(torch.nn.BatchNorm1d(hidden_size))
            modules_list.append(torch.nn.ReLU())
            if i == (layer_number-1):
                modules_list.append(torch.nn.Linear(hidden_size, num_classes)) #numclasses only for the last layer
            else:
                modules_list.append(torch.nn.Linear(hidden_size, hidden_size))

        self.model = torch.nn.Sequential(*modules_list)
        # ************************ end of sequential mode ************************ #

    def forward(self, x): #to define the forward pass
        # f(g(...)) ---> use the 3 lines below if you don't want to use the sequential mode
        # hidden = self.fc1(x)
        # relu = self.relu(hidden)
        # output = self.fc2(relu)
        return self.model(x)



#criterion is the LOSS function
def train_model(model, criterion, optimizer, epochs, X_train, y_train):
    model.train() #setting the model in train mode
    loss_values = []
    for epoch in range(epochs): # train loop, cicly on epoch
        optimizer.zero_grad() # set the gradient to 0, otherwise pytorch will accumulate it. Every backward pass we have to reset it to 0 

        # Forward pass (the whole training set)
        y_pred = model(X_train)


        # Compute Loss, calculated on my predictios
        loss = criterion(y_pred.squeeze(), y_train.squeeze()) #squeeze eliminates every extra dimension (e.g. if I have 20x1x1, it removes the 1)
        loss_values.append(loss.item())
        print('Epoch {} train loss: {}'.format(epoch, loss.item())) #.item save the loss calculate on all the training set

        # Backward pass
        loss.backward()
        optimizer.step() # apply the updates after that backward calculated them 

    return model, loss_values

#testing the model
def test_model(model, X_val, y_val):
    model.eval()#set to off dropout etc etc
    y_pred = model(X_val)
    y_pred = y_pred.argmax(dim=1, keepdim=True)

    """ 
        y_val.squeeze() cause y_val is 2D array so the score was calculated as matrix product,
        for this reason the score was much more bigger than 1
    """
    score = torch.sum((y_pred.squeeze() == y_val.squeeze()).float()) / y_val.shape[0]
    print('Test score', score.numpy())


# preprocessing funcion
def L1_norm(X):
    x_norm1 = np.linalg.norm(X, ord=1)
    x_normalized = X / x_norm1
    return x_normalized

# preprocessing funcion
def pca_(X_train, X_test, components):
    pca = decomposition.PCA(n_components=components)
    pca.fit(X_train)
    #print("pca.mean: ", pca.mean_)
    u = [round(i*100,2) for i in pca.explained_variance_ratio_]
    #print("pca.explained_variance_ratio: ",u)
    X_train_t = pca.transform(X_train)
    X_test_t = pca.transform(X_test)

    return X_train_t, X_test_t

# preprocessing funcion: z-score normalization
def standardization(X_train, X_val):
    X_train_mean = np.mean(X_train, axis=0)
    X_train_std = np.std(X_train, axis=0)

    X_train_s = (X_train - X_train_mean) / X_train_std
    X_val_s = (X_val - X_train_mean) / X_train_std

    return X_train_s, X_val_s

if __name__ == "__main__":
    #reading the dataset
    dataset = pd.read_csv("../binary_class_analysis/binary_class_no1020.csv", index_col=False,header=None)

    # loading the dataset with a DataFrame 
    dataset = pd.DataFrame(dataset) 

    # encode the classification column, si = 1 & no = 0
    dataset_label_encoded = encode_label(dataset)
    
    ############### REMOVE THE FIRST COLUMN BECAUSE IT'S THE INDEX ###############
    dataset = dataset.iloc[: , 1:]

    # splitting the dataset from classification label (the last one)
    X = pd.DataFrame(dataset.iloc[:,:-2])
    Y = pd.DataFrame(dataset.iloc[:,-1])

    # --------------- START PRE-PROCESSING ---------------
    # apply L1 normalization
    X = pd.DataFrame(L1_norm(np.array(X)))
    
    #train test split of the X and Y
    X_train, X_val, y_train, y_val = train_test_split(X.values, Y.values, test_size=0.2, random_state=42) # stratify=Y,
    
    # apply z-score normalization
    X_train, X_val = standardization(X_train, X_val)
    
    # --------------- END PRE-PROCESSING ---------------
    
    # ---------------------------- DATA VISUALIZATION ----------------------------
    # plt.subplot(1, 2, 1)
    # plt.hist(y_train, bins=len(np.unique(y_train)), histtype='bar', ec='black')
    # plt.subplot(1, 2, 2)
    # plt.hist(y_val, bins=len(np.unique(y_val)), histtype='bar', ec='black')
    # plt.show()

    num_classes = len(np.unique(Y)) # getting the number of class of Y
    hidden_size = 512 #number of neurons hidden size
    num_epochs = 2000 # number of epochs

    X_train = torch.FloatTensor(X_train) # for training data
    y_train = torch.LongTensor(y_train) # for the label (they are integers)
    X_val = torch.FloatTensor(X_val)
    y_val = torch.LongTensor(y_val)

    # getting the model
    model = Feedforward(X_train.shape[1], hidden_size, num_classes)
    criterion = torch.nn.CrossEntropyLoss() #Softmax and NNLL, does not require one-hot encoding of labels
    optimizer = torch.optim.SGD(model.parameters(), lr=0.001) 

    test_model(model, X_val, y_val)
    # training the model
    model, loss_values = train_model(model, criterion, optimizer, num_epochs, X_train, y_train)
    #calculate the accuracy of the model
    test_model(model, X_val, y_val)

    # plot of the loss for each epoch
    plt.plot(loss_values)
    plt.title("Number of epochs: {}".format(num_epochs))
    plt.show()
    




