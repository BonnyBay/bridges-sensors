from asyncio import SelectorEventLoop
from operator import mod
from pickle import LIST
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.model_selection import train_test_split
import pandas as pd
import torch
from torch.utils.data import Dataset, DataLoader, Subset #avrò dataloader diversi per subset diversi
from sklearn.preprocessing import LabelEncoder
import itertools
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler, SMOTE
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
import seaborn as sn
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import StandardScaler

#########################################################################
#     NEURAL NETWORK FEED FORWARD WITH STOCHASTIC GRADIENT DESCENT      #
#########################################################################



# confusion matrix for binary classification and multi classification
def make_confusion_matrix(Y_test, y_predicted):
    # ------------------------ use the line below for binary classification ------------------------
    label_categories = ['no', 'si'] #no or yes

    # ------------------------ use the line below for multiclass classification  ------------------------
    #label_categories = ['D_10', 'D_20', 'D_30', 'D_40', 'D_50', 'D_60', 'D_70', 'D_80', 'D_90', 'D_99']
    #label_categories = ['D_3040', 'D_5060', 'D_7080', 'D_9099']
    #label_categories = ['D_30','D_40', 'D_50','D_60', 'D_70','D_80', 'D_90','D_99']

    conf_matrix = confusion_matrix(Y_test, y_predicted)
    cmd_obj = ConfusionMatrixDisplay(conf_matrix, display_labels=label_categories)
    cmd_obj.plot()
    cmd_obj.ax_.set(
                title='Confusion Matrix Binary class', 
                xlabel='Predicted', 
                ylabel='True')

    plt.show()



#encoding the classification label
def encode_label(df):
    y_predict = LabelEncoder()
    df['y_encoded'] = y_predict.fit_transform(df.iloc[:,len(df.columns)-1])
    return df

def L1_norm(X):
    x_norm1 = np.linalg.norm(X, ord=2)
    x_normalized = X / x_norm1
    return x_normalized

def standardization(X_train, X_val):
    X_train_mean = np.mean(X_train, axis=0)
    X_train_std = np.std(X_train, axis=0)

    X_train_s = (X_train - X_train_mean) / X_train_std
    X_val_s = (X_val - X_train_mean) / X_train_std

    return X_train_s, X_val_s

class SensorsDataset(Dataset):
    def __init__(self):

        ############### USE ON REMOTE SLURM MACHINE ###############
        #dataset = pd.read_csv("/public.hpc/michele.bonini2/src/bridge_folder/binary_class_analysis/binaryclass_damage_dataset.csv", index_col=False,header=None)
        
        ############### USE ON LOCAL MACHINE ###############
        dataset = pd.read_csv("../binary_class_analysis/binary_class_no1020.csv", index_col=False,header=None)
        #dataset = pd.read_csv("../binary_class_analysis/binaryclass_damage_dataset.csv", index_col=False,header=None)
        #dataset = pd.read_csv("../multiclass_analysis/multiclass_all_damage_grouped.csv", index_col=False,header=None)
        #dataset = pd.read_csv("../multiclass_analysis/multiclass_all_damage.csv", index_col=False,header=None)
        #dataset = pd.read_csv("../multiclass_analysis/multiclass_no1020.csv", index_col=False,header=None)
    
        
        dataset = pd.DataFrame(dataset) 

        ############### REMOVE THE FIRST COLUMN BECAUSE IT'S DIRTY ###############
        dataset = dataset.iloc[: , 1:]

    
        # encode the classification column
        dataset = encode_label(dataset)
        #print(dataset) #to check the mean of 0 and 1

        X = pd.DataFrame(dataset.iloc[:,:-2]).values
        y = pd.DataFrame(dataset.iloc[:,-1:]).values


        self.num_classes = len(np.unique(y))
    
        X, y = self.preprocess(X,y)
        
        X = pd.DataFrame(X).values
        y = pd.DataFrame(y).values
        self.X = torch.FloatTensor(X)
        self.y = torch.LongTensor(y)

    def __len__(self): 
        return self.X.shape[0]

    def __getitem__(self, idx):
        return self.X[idx, :], self.y[idx] #X is the dataset, and getItem take an index (given from iterable)

    def balancing_dataset(self, X, y):
        #BALANCING of dataset UNDERSAMPLING
        print("BEFORE BALANCING ")
        print(np.unique(y, return_counts=True))
        #undersample = RandomUnderSampler(sampling_strategy='majority')
        #X, y = undersample.fit_resample(X, y)
        res = RandomOverSampler(random_state=42)
        X, y = res.fit_resample(X, y)
        print("AFTER BALANCING OVERRSAMPLING")
        print(np.unique(y, return_counts=True))
        return X, y

    def smote_balancing(self, X, y):
        print(np.unique(y, return_counts=True))
        smote = SMOTE(random_state = 101)
        X, y = smote.fit_resample(X, y)
        print(np.unique(y, return_counts=True))
        return X, y
    
    def preprocess(self,X, y): #se volessi fare preprocessin direttamente quando instanzio il dataset...
        #L1 norm
        #X = pd.DataFrame(L1_norm(np.array(X)))

        #balancing dataset
        #X, y = self.balancing_dataset(self, X,y)
        X, y = self.smote_balancing(X, y)
     
        return X, y
        

# define the model class feed forward
class Feedforward(torch.nn.Module): 
    def __init__(self, input_size, hidden_size, num_classes, layer_number):
        super(Feedforward, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_classes = num_classes

        modules_list = []
        modules_list.append(torch.nn.Linear(input_size, hidden_size))
        for i in range(0,layer_number): #layer_number min 1
            modules_list.append(torch.nn.BatchNorm1d(hidden_size))
            
            if i == (layer_number-1):
                modules_list.append(torch.nn.ReLU())
                modules_list.append(torch.nn.Linear(hidden_size, num_classes)) #numclasses only for the last layer
            else:
                modules_list.append(torch.nn.Linear(hidden_size, hidden_size))

        self.model = torch.nn.Sequential(*modules_list) # "*" IS REQUIRED BECAUSE: "list is not a Module subclass"


    def forward(self, x):
        return self.model(x)


def train_model(model, criterion, optimizer, epochs, train_loader, val_loader, device):

    # variable needed by early stopping technique
    the_last_loss = 100
    patience = 3 #number of epoch that I can wait
    trigger_times = 0

    loss_values = []
    for epoch in range(epochs): #loop on epochs
        model.train()

        for data, targets in train_loader: #loop on batch. DataLoader contain features and label
            
            data, targets = data.to(device), targets.to(device)
            optimizer.zero_grad()

            # Forward pass
            y_pred = model(data)
        
            # Compute Loss
            loss = criterion(y_pred.squeeze(), targets.squeeze())
            loss_values.append(loss.item())
            print('Epoch {} train loss: {}'.format(epoch, loss.item()))

            # Backward pass
            loss.backward()
            optimizer.step()

        divisione = np.mean(loss_values)
        print('\t\t MEAN loss for epoch {} : {}'.format(epoch,divisione))

        if epoch % 20 == 0:
            for g in optimizer.param_groups:
                g['lr'] = g['lr']*0.1

        # Early stopping
        # the_current_loss = val_model(model, device, val_loader,criterion)
        # print('The current loss:', the_current_loss)

        # if the_current_loss > the_last_loss:
        #     trigger_times += 1
        #     print('trigger times:', trigger_times)

        #     if trigger_times >= patience:
        #         print('Early stopping!\nStart to test process.')
        #         return model, loss_values

        # else:
        #     print('trigger times: 0')
        #     trigger_times = 0

        # the_last_loss = the_current_loss


    return model, loss_values

def val_model(model, device, valid_loader,criterion):
    # Settings
    model.eval()
    loss_total = 0

    # Test validation data
    with torch.no_grad():
        for data, targets in valid_loader:
            data, targets = data.to(device), targets.to(device)
            outputs = model(data)
            loss = criterion(outputs , targets.squeeze())
            loss_total += loss.item()    
    return loss_total / len(valid_loader)

# calculating AUC (area under the curve) and print ROC curve (Receiver Operating Characteristic)
def calculateAUC(y_test,y_pred):
    fpr, tpr, threshold = roc_curve(y_test, y_pred)
    roc_auc = auc(fpr, tpr)
    plt.title('Receiver Operating Characteristic')
    plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.show()



def test_model(model, data_loader, device): #data_loader è il val_loader
    model.eval()
    y_pred = []
    y_test = []
    for data, targets in data_loader:
        data, targets = data.to(device), targets.to(device)
        y_pred.append(model(data))
        y_test.append(targets)
    y_pred = torch.stack(y_pred).squeeze()
    y_test = torch.stack(y_test).squeeze()
    y_pred = y_pred.argmax(dim=1, keepdim=True)
    score = torch.sum((y_pred.squeeze() == y_test).float()) / y_test.shape[0]
    print('Test score ', score.cpu().numpy())
    print('f1 score ', f1_score(y_test, y_pred.squeeze(), average='binary'))
    make_confusion_matrix(y_test, y_pred.squeeze())
    calculateAUC(y_test, y_pred.squeeze())
    return score



if __name__ == "__main__":
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print("\n\n\t\t -------------------- DEVICE", device ," -------------------------\n\n\n")
    results_list = ""
    NUM_FINAL_LAYER = 6
    NUM_INITIAL_LAYER = 6

    hidden_sizes = [512]#[16, 32, 128, 256, 512]
    nums_epochs = [100]#[100, 300, 500, 700]
    batch_sizes = [256]#[16, 32, 64, 128, 256, 1024, 2048]
    learning_rate_hp = [0.01]#[0.01, 0.001]

    # generate the combination of hyperparameter
    hyperparameters = itertools.product(hidden_sizes, nums_epochs, batch_sizes, learning_rate_hp)


    dataset = SensorsDataset()
    dataset.preprocess(dataset.X, dataset.y)

    train_idx, test_idx = train_test_split(np.arange(len(dataset)), test_size=0.2, stratify=dataset.y, random_state=42)
    val_idx = np.arange(len(train_idx) - int(len(train_idx) * 0.8))

    train_subset = Subset(dataset, train_idx)
    test_subset = Subset(dataset, test_idx)
    val_subset = Subset(dataset, val_idx)
    
    # loop on epochs and batch
    for hidden_size, num_epochs, batch, learning_rate in hyperparameters:
        # settings for experiment replicability
        torch.manual_seed(42)
        np.random.seed(42)
        torch.use_deterministic_algorithms(True)
        
        # for every hyperparameters we shuffle the 3 set creating new train/val/test sets
        train_loader = DataLoader(train_subset, batch_size=batch, shuffle=True) # every epoch, shuffle the elements
        val_loader = DataLoader(val_subset, batch_size=batch, shuffle=True)
        test_loader = DataLoader(test_subset, batch_size=1, shuffle=True)

        for i in range(NUM_INITIAL_LAYER,NUM_FINAL_LAYER+1): # 'i' is the number of layer at each cycle
            model = Feedforward(dataset.X.shape[1], hidden_size, dataset.num_classes,i) 
            model.to(device)
            criterion = torch.nn.CrossEntropyLoss()
            optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, momentum=0.99)

            #scoreTrain = test_model(model, test_loader, device)
            model, loss_values = train_model(model, criterion, optimizer, num_epochs, train_loader, val_loader, device)
            scoreTest = test_model(model, test_loader, device)

            # ------------------- DATA VISUALIZATION OF LOSS -------------------------
            plt.plot(loss_values)
            plt.title("Number of epochs: {}".format(num_epochs))
            plt.show()

            plt.plot(loss_values)
            plt.title("Number of epochs: {} - Hidden size: {} - Mini batch: {}".format(num_epochs, hidden_size, batch ))
            plt.show()
            plt.savefig('loss_all_damages.png')
            # ------------------- END OF DATA VISUALIZATION OF LOSS ------------------- 

            results_list+="Device: {} - MODEL with {} layer - Number of epochs: {} - Hidden size: {} - Mini batch: {} - Lr: {} \n - Test Score Before Training: {} - Test Score: {}\n".format(device,i,num_epochs, hidden_size, batch, learning_rate, scoreTrain, scoreTest)
        print(results_list)

        #open text file

        ############### USE ON REMOTE SLURM MACHINE ###############
        # text_file = open("/public.hpc/michele.bonini2/src/bridge_folder/binary_class_analysis/out_grouped_multi.txt", "w")
        # text_file = open("/public.hpc/michele.bonini2/src/bridge_folder/binary_class_analysis/FINALE_loss_plot.txt", "w")

        # ############### USE ON LOCAL MACHINE ###############
        # #text_file = open("../binary_class_analysis/results/allDamage.txt", "w")

        # #write string to file
        # text_file.write(results_list)
        
        # #close file
        # text_file.close()




