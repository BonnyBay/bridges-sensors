from operator import concat
import pandas as pd

# correct wrong float number syntax
def find_second_point(err_number):

    # rindex return the index of the last position of the char in the string
    dot_to_cancel = err_number.rindex('.')
    #return the correct value
    return err_number[0:dot_to_cancel]

#group some damage to each other. e.g. damage of 10 and damage of 20 become damage of 1020
def group_damage(): #to improve the accuracy cause we have few data
    concat_dataset = pd.read_csv("../../multiclass_analysis/multiclass_all_damage.csv", index_col=False,header=None)
    concat_dataset.iloc[:,63:] = concat_dataset.iloc[:,63:].replace(['D_10', 'D_20'], 'D_1020')
    concat_dataset.iloc[:,63:] = concat_dataset.iloc[:,63:].replace(['D_30', 'D_40'], 'D_3040')
    concat_dataset.iloc[:,63:] = concat_dataset.iloc[:,63:].replace(['D_50', 'D_60'], 'D_5060')
    concat_dataset.iloc[:,63:] = concat_dataset.iloc[:,63:].replace(['D_70', 'D_80'], 'D_7080')
    concat_dataset.iloc[:,63:] = concat_dataset.iloc[:,63:].replace(['D_90', 'D_99'], 'D_9099')
    # remove 10 and 20 damages from the grouped dataset
    concat_dataset = concat_dataset.loc[concat_dataset.iloc[:,64] != 'D_1020']
    concat_dataset.to_csv("../../multiclass_analysis/multiclass_all_damage_grouped.csv", index=False, header=False)

def concat_dataset():

    # sticks
    A = [1, 6, 17, 18]
    # damages
    D = [10,20,30, 40, 50, 60, 70, 80, 90, 99] 
    # read the first dataset with A=1 and D=10 as concatenation base
    concat_dataset = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_A"+str(A[0])+"_D"+str(D[0])+"_transpose.csv", index_col=False,header=None)
    # take all the coumn but last because the last is the label for the binary classification
    concat_dataset = concat_dataset.iloc[:,:-1]
    # iterate over the datasets to take all the sample in one unique csv
    for i in A:
        for j in D:
            damage = "D_"+str(j)
            # if the first dataset -> not concat 
            if (i == A[0] and j==D[0]):
                # saving the damages in the last column of the dataframe as label
                concat_dataset['damage'] = pd.Series([damage for i in range(100)])
            # else concat the other dataset to the first
            else:
                file_data = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_A"+str(i)+"_D"+str(j)+"_transpose.csv", index_col=False,header=None)
                file_data = file_data.iloc[:,:-1]
                # adding damage type label to the dataframe contac
                file_data['damage'] = pd.Series([damage for i in range(100)])
                # concat two dataset each time
                concat_dataset = pd.concat([concat_dataset, file_data])

    # round the decimal number to 2 to make faster the computation
    concat_dataset = concat_dataset.round(decimals=2)
    # write the final dataframe to a csv file
    concat_dataset.to_csv("../../multiclass_analysis/multiclass_all_damage.csv", index=False, header=False)
    
    """
        We notice that the first column have some incorrect values like '18.452.1' 
        and we correct them cutting of the digits after the second point
    """

    # reading the dataset concatendated
    df = pd.read_csv("../../multiclass_analysis/multiclass_all_damage.csv", index_col=False, header=None)
    #taking the first column
    first_column = df.iloc[:,0]
    i = 0
    # itarate over the first column values and correct them if i can't cast them to float
    for el in first_column:
        try:
            float(el)
        except:
            new_el = find_second_point(el)
            first_column[i] = new_el
        i += 1

    # recreate the first column with correct float values
    df.iloc[:,0] = first_column

    # write the correct csv to the file
    df.to_csv("../../multiclass_analysis/multiclass_all_damage.csv", index=False, header=False)

if __name__ == "__main__":
    #concat_dataset()
    group_damage()




