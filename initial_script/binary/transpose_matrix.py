import os
import pandas as pd


"""
    CREATE ALL THE INITIAL DATASETS TRANSPOSED -> COLUMNS TO ROWS
"""

# rods
A = [1, 6, 17, 18]
# damages
D = [10,20,30,40, 50, 60, 70, 80, 90, 99]

# for every rods e damage type, create the transpose in a new csv file
for i in A:
     for j in D:
        # reading the respective dataset 
        df = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_A"+str(i)+"_D"+str(j)+".csv")
        # transpose the dataset with the T function
        transpose = df.T
        transpose = pd.DataFrame(transpose)
        # write the new transpose dataset in a csv
        transpose.to_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_A"+str(i)+"_D"+str(j)+"_transpose.csv", header=False)

# transpose the health dataset and write to a new csv
df = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_H.csv")
transpose = df.T
transpose = pd.DataFrame(transpose)
transpose.to_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_H_transpose.csv", header=False)