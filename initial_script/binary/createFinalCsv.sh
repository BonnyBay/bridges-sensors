#!/bin/bash

####################################################################
#      RUN THIS SCRIPT TO CREATE THE CORRECT CSV FILE              #
####################################################################

#transpose the matrix
python transpose_matrix.py

#add the label on binary csv
python add_class_isbreak.py

#merge every csv with each other
python concat_csv.py

#remove data error if exist from the first column such as: '12.3.123'
python remove_data_error.py

#merge the dataset with health samples
python script_for_H_Large.py

