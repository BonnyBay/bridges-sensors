import pandas as pd

"""
    TRANSPOSE AND CONCAT THE H LARGE DATASET TO THE DATASET CONCATENATED BEFORE WITH THE DAMAGED DATASETS
"""

#read the health large dataset, traspose the csv and assign the labal as not damaged  
df = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_H_large.csv", index_col=False,header=None)
transpose = df.T
transpose = pd.DataFrame(transpose)
is_break = ["no" for i in range(4000)]
transpose.loc[: ,64] = is_break

# concat the H large dataset with the damaged dataset concatenated before
concat_dataset = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/FinalVersion_DICAM_TOWER_CONCAT.csv", index_col=False,header=None)
concat_dataset = pd.concat([concat_dataset, transpose])

#round the dataset
concat_dataset = concat_dataset.round(decimals=4)

#save the new final csv with health dataset concatenated
concat_dataset.to_csv("../../DICAM_TOWER_support_freq_transpose/FinalVersion_DICAM_TOWER_CONCAT.csv", header=False)