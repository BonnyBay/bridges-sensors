import pandas as pd


"""
    CONCAT THE DATASETS IN A UNIQUE ONE
"""

# aste
A = [1, 6, 17, 18]
# danni
D = [ 10,20,30, 40, 50, 60, 70, 80, 90, 99]

# read the first dataset with A=1 and D=10 as concatenation base
concat_dataset = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_A"+str(A[0])+"_D"+str(D[0])+"_transpose.csv", index_col=False,header=None)

# iterate over the datasets to take all the sample in one unique csv
for i in A:
    for j in D:
        # if the first dataset -> not concat 
        if (i==A[0] and j==D[0]): 
            print("e.g. I do not merge A==1 and D==10") 

        # else concat the other dataset to the first
        else:
            file_data = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_A"+str(i)+"_D"+str(j)+"_transpose.csv", index_col=False,header=None)
            # concat two dataset each time
            concat_dataset = pd.concat([concat_dataset, file_data])


#concat the damaged dataset with health dataset
health_dataset = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_H_transpose.csv", index_col=False,header=None)
concat_dataset = pd.concat([concat_dataset, health_dataset])
#concat_dataset = concat_dataset.iloc[: , :-1]
concat_dataset=concat_dataset.round(decimals=4)#THIS IMPROVE THE PERFORMANCES
#export to csv the final dataset
concat_dataset.to_csv("../../DICAM_TOWER_support_freq_transpose/FinalVersion_DICAM_TOWER_CONCAT.csv", index=False, header=False)
print(concat_dataset)
