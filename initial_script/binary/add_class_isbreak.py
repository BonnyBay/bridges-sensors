import pandas as pd

"""
    CODE TO ADD THE IS BROKEN LABEL TO THE BYNARY DATASET -> "si" if it is broken, "no" otherwise
"""

# rods
A = [1, 6, 17, 18]
# damages
D = [10,20,30, 40, 50, 60, 70, 80, 90, 99]

# iterate over the datasets to add the label is damaged
for i in A:
    for j in D:
        df = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_A"+str(i)+"_D"+str(j)+"_transpose.csv", index_col=False,header=None)
        # creating the label vector to assign to the last column of the dataset
        is_break = ["si" for i in range(100)]
        df['is_break'] = is_break
        # writing the new dataset to the csv
        df.to_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_A"+str(i)+"_D"+str(j)+"_transpose.csv", index=False, header=False)


# adding the label not damaged to the H dataset and rewrite the csv
df = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_H_transpose.csv", index_col=False,header=None)
is_break = ["no" for i in range(100)]
df['is_break'] = is_break
df.to_csv("../../DICAM_TOWER_support_freq_transpose/DICAM_TOWER_H_transpose.csv", index=False, header=False)