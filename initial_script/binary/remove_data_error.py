import pandas as pd

"""
    We notice that the first column have some incorrect values like '18.452.1' 
    and we correct them cutting of the digits after the second point
"""

df = pd.read_csv("../../DICAM_TOWER_support_freq_transpose/FinalVersion_DICAM_TOWER_CONCAT.csv", index_col=False,header=None)

# function to remove the float error values like '18.435.1'
def find_second_point(err_number):

    #rindex return the last index of the given string
    dot_to_cancel = err_number.rindex('.')

    #return the corrected element
    return el[0:dot_to_cancel]

#take the first column where are the wrong float values
first_column = df.iloc[:][0]
i = 0

# iterate over this wrong float values to correct them
for el in first_column:
    try:
        float(el)
    except:
        #correct the value and assign it to his position in the column
        new_el = find_second_point(el)
        first_column[i] = new_el
    i += 1
        
#sobstitute the first column of the dataet with the new corrected one
df.iloc[:][0] = pd.Series(first_column)

# rewrite the dataset with the corrected column on the csv
df.to_csv("../../DICAM_TOWER_support_freq_transpose/FinalVersion_DICAM_TOWER_CONCAT.csv", index=False, header=False)






